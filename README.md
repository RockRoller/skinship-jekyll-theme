# skinship-jekyll-theme

A jekyll theme for all [skinship websites](https://skinship.xyz). Ships with layouts, icons, assets, includes, etc

## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "skinship-jekyll-theme"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: skinship-jekyll-theme
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install skinship-jekyll-theme

TODO: figure out if usage instructions above are correct for the way this will be used.

## Usage

TODO: Write usage instructions here. Describe your available layouts, includes, sass and/or assets.

The following elements must be provided by the site:

-   `includes/head.html` containing all the site specific content that should go in the head. the `<head>` tag must not be included
-   `oEmbed.json`
-   various mandatory theme configurations inside `_config.yml`

```yml
theme_config:
    gitlab_url: url
    privacy_page: relative url
    project: "compendium" OR "skinship-mainsite"
```

-   `_data/header.yml` containing a list of header object items. The icon key must refer to an icon in `_includes/icons/<icon_name>.svg`. The list of entries will be relative to their parent category. The `highlight` key is responsible for matching the active page in the header

```yml
- name: "Home"
  url: "/"
  icon: "home"
  highlight: "home"
- name: "Category 1"
  url: "/category-path/"
  icon: "category-icon"
  highlight: "category1"
  entries:
      - name: "Entry 1"
        url: "1"
      - name: "Entry 2"
        url: "2"
```

- page level `show_announcement` keys. This determines if announcements should be shown on that page
- `/assets/css/main.css`
