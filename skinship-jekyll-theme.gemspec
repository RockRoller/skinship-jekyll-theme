# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "skinship-jekyll-theme"
  spec.version       = "0.1.45"
  spec.authors       = ["RockRoller01"]
  spec.email         = ["skinship.osu@gmail.com"]

  spec.summary       = "jekyll theme used for all skinship websites"
  spec.homepage      = "https://gitlab.com/skinship/skinship-jekyll-theme"
  spec.license       = ""

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.2"
end
